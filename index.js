const botconfig = require("./botconfig.json");
const auth = require("./auth.json");
const Discord = require("discord.js");
const sqlite3 = require('sqlite3').verbose();

const bot = new Discord.Client({disableEveryone: true});
const swears = ['ass', 'asshole', 'bitch', 'bullshit', 'cock', 'cocksucker', 
                'cunt', 'damn', 'damnit', 'dick', 'dickhead', 
                'fuck', 'fucker', 'fuckin', 'fucking', 'motherfucker', 
                'shit', 'shithead'];


let db = new sqlite3.Database('./db/swears.db', (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Connected to the swears database.');
});


const detectSwear = function(word) {
    if(swears.indexOf(word.toLowerCase()) > -1){
        return true;
    } else {
        return false;
    }
}

const updateActivity = function(){
    let serverCount = bot.guilds.keyArray().length;
    bot.user.setActivity("swears on " + serverCount + " servers.", { type: 'LISTENING' });
}

const addUsersToDB = function(params){
    let sql = `insert into users(user_id, guild_id, swear, count) VALUES (?, ?, ?, 0)`

    for(let i=0; i<swears.length; i++){
        params[2] = swears[i];
        db.run(sql, params, (err) => {
            if (err) {
                console.error(err.message);
            }
        });
    }
}

const incrementSwear = function(params){
    let sql = `UPDATE users
               SET count = count + 1
               WHERE user_id = ? AND guild_id = ? AND swear = ?`
    
    db.run(sql, params, (err) => {
        if(err) {
            console.log(err.message);
        }
    });
}

const getUserStats = function(message){ 
    let User = {};
    let sql = `select count count, swear swear FROM users WHERE user_id = ? AND guild_id = ? ORDER BY count DESC`
    let totalSwears = 0;

    db.each(sql, [message.author.id, message.guild.id], (err, row) => {
        if(err){
            throw err;
        } else {
            User[row.swear] = row.count;
            totalSwears += row.count;
        }
    }, (err, count) => {
        if(err){
            throw err;
        } else {
            let userProperty = Object.keys(User);
            User["totalSwears"] = totalSwears;
            let icon = message.author.displayAvatarURL;
            let embed = new Discord.RichEmbed()
            .setAuthor(message.author.username + "'s Swear Stats", bot.user.displayAvatarURL)
            .setColor('RANDOM')
            .setThumbnail(icon)
            .addField("Total Swears", User.totalSwears)
            .addField("Top 5 Swears",
                        userProperty[0] + ": " + User[userProperty[0]] + '\n' +
                        userProperty[1] + ": " + User[userProperty[1]] + '\n' +
                        userProperty[2] + ": " + User[userProperty[2]] + '\n' +
                        userProperty[3] + ": " + User[userProperty[3]] + '\n' +
                        userProperty[4] + ": " + User[userProperty[4]]);
            return message.channel.send(embed);
            
        }
    });
}


const getServerStats = function(message){
    let sql = `select count count, swear swear, user_name user_name FROM users WHERE guild_id = ? ORDER BY count DESC`
    let totalSwears = 0;
    let topSwears = {};
    let topSwearers = {};

    db.each(sql, [message.guild.id], (err, row) => {
        if(err){
            throw err;
        } else {
            totalSwears += row.count;
            if(topSwears[row.swear]){
                topSwears[row.swear] += row.count;
            } else {
                topSwears[row.swear] = row.count;
            }

            if(topSwearers[row.user_name]){
                topSwearers[row.user_name] += row.count;
            } else {
                topSwearers[row.user_name] = row.count;
            }
        }
    }, (err, count) => {
        if(err){
            throw err;
        } else {
            let topSwearerProperty = Object.keys(topSwearers);
            let topSwearsProperty = Object.keys(topSwears);

            let icon = message.guild.iconURL;
            let embed = new Discord.RichEmbed()
            .setAuthor(message.guild.name + "'s Swear Stats", bot.user.displayAvatarURL)
            .setColor('RANDOM')
            .setThumbnail(icon)
            .addField("Total Swears", totalSwears)
            .addField("Top 5 Swears", 
                    topSwearsProperty[0] + ": " + topSwears[topSwearsProperty[0]] + '\n' +
                    topSwearsProperty[1] + ": " + topSwears[topSwearsProperty[1]] + '\n' +
                    topSwearsProperty[2] + ": " + topSwears[topSwearsProperty[2]] + '\n' +
                    topSwearsProperty[3] + ": " + topSwears[topSwearsProperty[3]] + '\n' +
                    topSwearsProperty[4] + ": " + topSwears[topSwearsProperty[4]],
                    true)
            .addField("Top 5 Filthy Mouthed Sailors", 
                    topSwearerProperty[0] + ": " + topSwearers[topSwearerProperty[0]] + '\n' +
                    topSwearerProperty[1] + ": " + topSwearers[topSwearerProperty[1]] + '\n' +
                    topSwearerProperty[2] + ": " + topSwearers[topSwearerProperty[2]] + '\n' +
                    topSwearerProperty[3] + ": " + topSwearers[topSwearerProperty[3]] + '\n' +
                    topSwearerProperty[4] + ": " + topSwearers[topSwearerProperty[4]],
                    true);
            return message.channel.send(embed);
        }
    });
}

bot.on("ready", async() => {
    console.log(`${bot.user.username} has entered the building!`)
    db.run('CREATE TABLE IF NOT EXISTS users(user_name TEXT, user_id TEXT, guild_id TEXT, count INTEGER, swear TEXT, PRIMARY KEY(user_id, guild_id, swear))');
    updateActivity();

    bot.guilds.forEach(function(guild){
        guild.members.forEach(function(member, memberId){
            addUsersToDB([memberId, guild.id])
        })
    })
});

bot.on("message", async message => {
    if(message.author.bot) return;
    if(message.channel.type === "dm") return;

    //swear detection
    let messageArray = message.content.replace(/[^a-zA-Z ]/g, "").split(" ");
    for(word in messageArray){

        //detect plurals and remove the s
        if(messageArray[word].slice(-1) === 's' && messageArray[word].toLowerCase() !== 'ass'){
            slicedWord = messageArray[word].substring(0, messageArray[word].length - 1)
        } else {
            slicedWord = messageArray[word];
        }
        if(detectSwear(slicedWord)){
            //react to the message with the swear
            message.react('🤑');

            //update database
            incrementSwear([message.author.id, message.guild.id, slicedWord.toLowerCase()]);
        }
    };

    //command detection
    let commandArray = message.content.split(" ");
    let command = commandArray[0];
    let args = commandArray.slice(1);

    //user stats
    if(command === botconfig.prefix + "stats" && !args[0]){
        getUserStats(message);
    }

    //server stats
    if(command === botconfig.prefix + "stats" && args[0] === "server") {
        getServerStats(message);
    }

    //help
    if(command === botconfig.prefix + "help" && !args[0]){
        message.reply("You just say bad words and I count them. `$stats` to see your stats. `$stats server` to see this server's stats.")
    }

});

bot.on("guildCreate", async guild => {
    updateActivity();
    guild.members.forEach(function(member, memberId){
        addUsersToDB([memberId, guild.id]);
    })
});

bot.on("guildDelete", async() => {
    updateActivity();
});

bot.on("guildMemberAdd", async member => {
    addUsersToDB([member.user.id, member.guild.id]);
});

bot.login(auth.token);